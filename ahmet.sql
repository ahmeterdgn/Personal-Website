-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 06 Tem 2019, 00:06:24
-- Sunucu sürümü: 10.1.37-MariaDB
-- PHP Sürümü: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `denemeler`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `bildiri`
--

CREATE TABLE `bildiri` (
  `id` int(11) NOT NULL,
  `baslik` varchar(150) COLLATE utf8_turkish_ci NOT NULL,
  `icerik` varchar(300) COLLATE utf8_turkish_ci NOT NULL,
  `tarih` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `bildiri`
--

INSERT INTO `bildiri` (`id`, `baslik`, `icerik`, `tarih`) VALUES
(1, 'Geliştirilmekte! ', 'Bu web sitesi hala geliştirilme aşamasındadır. Sayfalarda hatalar olabilir. Herhangi bir bulursanız, lütfen onları <a href=\'mailto:ahmeterdoga46@gmail.com\'>ahmeterdoga46@gmail.com </a>adresine bildirin', '2019-05-31 20:48:12');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `resimrul` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `tarih` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `images`
--

INSERT INTO `images` (`id`, `resimrul`, `tarih`) VALUES
(6, 'https://resmim.net/f/5l2l72.jpg?nocachealt', '2019-06-11 11:47:55');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `kullanıcılar`
--

CREATE TABLE `kullanıcılar` (
  `id` int(11) NOT NULL,
  `email` varchar(150) COLLATE utf8_turkish_ci NOT NULL,
  `sifre` varchar(300) COLLATE utf8_turkish_ci NOT NULL,
  `tarih` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `kullanıcılar`
--

INSERT INTO `kullanıcılar` (`id`, `email`, `sifre`, `tarih`) VALUES
(1, 'ahmet@ahmet.com', 'cdb5efc9c72196c1bd8b7a594b46b44f', '2019-05-31 19:54:05');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `projeler`
--

CREATE TABLE `projeler` (
  `id` int(11) NOT NULL,
  `projename` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `projeinfo` text COLLATE utf8_turkish_ci NOT NULL,
  `projeimgsmall` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `projeimg1` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `projeimg2` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `projeimg3` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `projeimg4` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `udatetime` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `boyut` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `version` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `indirme` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `begeni` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `url` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `tarih` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `projeler`
--

INSERT INTO `projeler` (`id`, `projename`, `projeinfo`, `projeimgsmall`, `projeimg1`, `projeimg2`, `projeimg3`, `projeimg4`, `udatetime`, `boyut`, `version`, `indirme`, `begeni`, `url`, `tarih`) VALUES
(1, 'Muzideo Downloander', 'Youtube Üzerinden Mp3 Ve Mp4 İndirmek İçin İdeal Bir Müzik İndiricisidir.\r\n<h3> Özellikleri:</h3> <br>\r\n -> URL ARACILIĞI İLE İNDİRME<br>\r\n->WEB VİEWER ARACILIĞI İLE İNDİRME<br>\r\n-> AYARLAR BÖLÜMÜNDEN İNDİRECEĞİNİZ DOSYA DİZİNİNİ BELİRLEME <br>\r\n-> REKLAMSIZ<br>\r\n-> YOUTUBE HESABINIZA GİRİŞ YAPARAK DİNLEDİĞİNİZ MÜZİKLERİ RAHATLIKLA BULUP İNDİRE BİLİRSİNİZ<br>', 'https://resmim.net/f/Z7fWtw.png?nocache', 'https://resmim.net/f/U6P6sU.png', 'https://resmim.net/f/gOUPVP.png', 'https://resmim.net/f/A0hrXU.png', 'https://resmim.net/f/3Lswnc.png', '02.06.2019', '6.5 MB', '1.0', '+10', '5', 'https://yadi.sk/d/CmjN1hP7YLqFnA', '2019-06-02 15:25:09');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `resim`
--

CREATE TABLE `resim` (
  `resim1` varchar(250) COLLATE utf8mb4_turkish_ci NOT NULL,
  `resim2` varchar(250) COLLATE utf8mb4_turkish_ci NOT NULL,
  `resim3` varchar(250) COLLATE utf8mb4_turkish_ci NOT NULL,
  `tarih` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_turkish_ci;

--
-- Tablo döküm verisi `resim`
--

INSERT INTO `resim` (`resim1`, `resim2`, `resim3`, `tarih`) VALUES
('https://resmim.net/f/KA0sit.png', 'https://resmim.net/f/KA0sit.png', 'https://resmim.net/f/KA0sit.png', '2019-05-31 21:13:02');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `sitebilgileri`
--

CREATE TABLE `sitebilgileri` (
  `id` int(11) NOT NULL,
  `kadi` varchar(150) COLLATE utf8_turkish_ci NOT NULL,
  `jop` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `resim` varchar(300) COLLATE utf8_turkish_ci NOT NULL,
  `bio1` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `bio2` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `bio3` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `bio4` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `hakkinda` text COLLATE utf8_turkish_ci NOT NULL,
  `tarih` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `sitebilgileri`
--

INSERT INTO `sitebilgileri` (`id`, `kadi`, `jop`, `resim`, `bio1`, `bio2`, `bio3`, `bio4`, `hakkinda`, `tarih`) VALUES
(1, 'Ahmet Erdoğan', 'Software Engineer', 'https://resmim.net/f/KA0sit.png', 'Security Expert ', 'Android Expert', 'Web Developer', 'Accounting student', 'Öncelikle Merhaba Benim Adım Ahmet, 18 Yaşındayım Ve 2016 Yılından İtibaren  Web Geliştirme Ve Mobil Programlama İle İlgilenmekteyim. Bir Çok Projeye İmza Attım Mobil Ve Web Olarak Projelerimin  Sadece Bir Kaç Tanesini Projeler Bölümünde Bula Bilirsiniz.', '2019-05-31 20:42:04');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `sosyalmedya`
--

CREATE TABLE `sosyalmedya` (
  `id` int(11) NOT NULL,
  `instagram` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `facebook` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `twitter` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `github` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `medium` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `codepen` varchar(250) COLLATE utf8_turkish_ci NOT NULL,
  `tarih` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `sosyalmedya`
--

INSERT INTO `sosyalmedya` (`id`, `instagram`, `facebook`, `twitter`, `github`, `medium`, `codepen`, `tarih`) VALUES
(1, 'https://instagram.com/ahmeterdgn.js', 'https://facebook.com/ahmeterdgn49', 'https://twitter.com/ahmeterdgn49', 'https://github.com/ahmeterdgn', 'https://medium.com/@ahmeterdgn49', 'https://codepen.io/ahmeterdgn49', '2019-05-31 20:57:24');

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `bildiri`
--
ALTER TABLE `bildiri`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `kullanıcılar`
--
ALTER TABLE `kullanıcılar`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `projeler`
--
ALTER TABLE `projeler`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `sitebilgileri`
--
ALTER TABLE `sitebilgileri`
  ADD PRIMARY KEY (`id`);

--
-- Tablo için indeksler `sosyalmedya`
--
ALTER TABLE `sosyalmedya`
  ADD PRIMARY KEY (`id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `bildiri`
--
ALTER TABLE `bildiri`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Tablo için AUTO_INCREMENT değeri `kullanıcılar`
--
ALTER TABLE `kullanıcılar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `projeler`
--
ALTER TABLE `projeler`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `sitebilgileri`
--
ALTER TABLE `sitebilgileri`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Tablo için AUTO_INCREMENT değeri `sosyalmedya`
--
ALTER TABLE `sosyalmedya`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
