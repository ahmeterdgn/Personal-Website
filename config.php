<?php

  $db_sunucu ="localhost";
  $db_user = "root";
  $db_name ="denemeler";
  $db_pass="";
  try {
    $db_conn=new PDO("mysql:host={$db_sunucu};dbname={$db_name}",$db_user, $db_pass);
    $db_conn->exec("set names utf8");
    $db_conn-> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $a) {
    echo $a->getMessage();
  }
?>
