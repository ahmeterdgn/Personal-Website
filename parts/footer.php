<?php
  include 'config.php';
  $sorgu = $db_conn->query("SELECT * FROM sitebilgileri");
  $rows= $sorgu->fetch(PDO::FETCH_ASSOC);
  $sorgu2 = $db_conn->query("SELECT * FROM sosyalmedya");
  $rows2= $sorgu2->fetch(PDO::FETCH_ASSOC);
 ?>

 <!-- Footer -->
 <footer class="page-footer font-small color-dark pt-4">

   <!-- Footer Elements -->
   <div class="container">

     <!-- Social buttons -->
     <ul class="list-unstyled list-inline text-center">
       <li class="list-inline-item">
         <a class="btn-floating btn-ins mx-1" href=" <?php echo $rows2['instagram'] ?>">
           <i class="fab fa-instagram"> </i>
         </a>
       </li>
       <li class="list-inline-item">
         <a class="btn-floating btn-tw mx-1" href=" <?php echo $rows2['twitter'] ?>">
           <i class="fab fa-twitter"> </i>
         </a>
       </li>
       <li class="list-inline-item">
         <a class="btn-floating btn-fb mx-1" href=" <?php echo $rows2['facebook'] ?>">
           <i class="fab fa-facebook"> </i>
         </a>
       </li>
       <li class="list-inline-item">
         <a class="btn-floating btn-git mx-1" href=" <?php echo $rows2['github'] ?>">
           <i class="fab fa-github"> </i>
         </a>
       </li>
       <li class="list-inline-item">
         <a class="btn-floating btn-email mx-1" href=" <?php echo $rows2['medium'] ?>">
           <i class="fab fa-medium"> </i>
         </a>
       </li>
       <li class="list-inline-item">
         <a class="btn-floating btn-git mx-1" href=" <?php echo $rows2['codepen'] ?>">
           <i class="fab fa-codepen"> </i>
         </a>
       </li>
     </ul>
     <!-- Social buttons -->

   </div>
   <!-- Footer Elements -->

   <!-- Copyright -->
   <div class="footer-copyright text-center py-3" id="yil">

   </div>
   <!-- Copyright -->
   <script type="text/javascript">
     var date;
date =new Date().getFullYear();
document.getElementById('yil').innerHTML="<a href='#'> <?php echo $rows['kadi']?></a> &copy; " +date+" Tüm Hakları Saklıdır.";

   </script>
 </footer>
 <!-- Footer -->
<!-- Footer -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js"></script>

<script src="https://code.jquery.com/jquery-3.4.1.min.js" charset="utf-8"></script>
<script type="text/javascript" src="js/footer.js"></script>
<script type="text/javascript" src="js/mdb.js"></script>
<script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
<script src="js/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
