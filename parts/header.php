<?php
  $sorgu = $db_conn->query("SELECT * FROM sitebilgileri");
  $rows= $sorgu->fetch(PDO::FETCH_ASSOC);
 ?>


 <title><?php echo $rows['kadi']; ?></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta http-equiv="x-ua-compatible" content="ie=edge">
   <meta name="description" content="<?php echo $rows['kadi']; ?>">
   <meta name="keywords" content="Ahmet,Erdoğan, Ahmet Erdoğan" />
   <meta property="fb:admins" content="<?php echo $rows['kadi']; ?>" />
   <meta property="og:title" content="<?php echo $rows['kadi']; ?>" />
   <meta property="og:type" content="website" />
   <meta property="og:url" content="https://github.com/ahmeterdgn49" />
   <meta property="og:image" content="https://avatars2.githubusercontent.com/u/48730205?s=460&v=4" />
   <meta property="og:description" content="<?php echo $rows['kadi']; ?> Kişisel Sayfası" />
  <link rel="shortcut icon" type="image/png" href="<?php echo $rows['kadi']; ?>"/>
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/master.css">
    <link rel="stylesheet" href="css/mdb.css">
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
<link rel="stylesheet" type="text/css" href="https://mdbootstrap.com/wp-content/themes/mdbootstrap4/css/compiled-4.8.1.min.css">
</head>
<body>
  <!--MENÜ-->
  <div>
<nav class="navbar navbar-expand-lg navbar navbar-dark bg-dark fixed-top">
  <a class="navbar-brand" href="anasayfa"><?php echo $rows['kadi']; ?></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link hbtn hb-border-bottom-br4 hpad2 hbor2 " href="anasayfa"><i class="fas fa-home"></i> Ana Sayfa</a>
      <a class="nav-item nav-link hbtn hb-border-bottom-br4 hpad2 hbor2" href="about"><i class="fas fa-user"></i> Hakkında</a>
      <a class="nav-item nav-link hbtn hb-border-bottom-br4 hpad2 hbor2" href="project"><i class="fas fa-briefcase"></i> Projeler</a>
      <a class="nav-item nav-link hbtn hb-border-bottom-br4 hpad2 hbor2" href="gallery"><i class="fas fa-images"></i> Gallery</a>
    </div>
  </div>
</nav>
<script src="js/jquery-3.4.1.min.js"></script>
<script type="application/javascript">
     console.log(window.location.pathname.substring(1));
    $("a[href="+window.location.pathname.substring(1)+"]").addClass('active');
</script>
<br><br><br>
