

<?php
  $sorgu = $db_conn->query("SELECT * FROM bildiri");
  $rows= $sorgu->fetch(PDO::FETCH_ASSOC);
  ?>

<div class="alert alert-success alert-dismissible fade show" role="alert">
  <strong><?php echo $rows['baslik']; ?></strong>
  <?php echo $rows['icerik']; ?>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
